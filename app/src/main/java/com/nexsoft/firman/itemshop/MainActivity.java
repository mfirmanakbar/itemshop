package com.nexsoft.firman.itemshop;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Integer> ar_no = new ArrayList<Integer>();
    private ArrayList<String> ar_nama = new ArrayList<String>();
    private String DataText = "1|Bubur|End^2|Ayam|End^3|Sate|End^4|Pisang|End^5|Es Krim|End^6|Apel|End^7|Jeruk|End^8|Lemon|End^9|Sirsak|End^";
    private AdapterItems adapterItems;
    private ListView list;
    private List<DataItems> itemList = new ArrayList<DataItems>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list    = (ListView) findViewById(R.id.ListViewItems);

        adapterItems = new AdapterItems(MainActivity.this, itemList);
        list.setAdapter(adapterItems);

        simpanKeArray();
    }

    private void simpanKeArray() {
        String[] itemInduk = DataText.toString().trim().split("\\^");
        for (int a=0; a<itemInduk.length; a++){
            String[] itemAnak = itemInduk[a].toString().trim().split("\\|");
            ar_no.add(Integer.valueOf(itemAnak[0]));
            ar_nama.add(itemAnak[1]);
        }
        lihatHasilArray();
    }

    private void lihatHasilArray() {
        for (int i=0; i<ar_no.size(); i++){
            Log.d(
                "COBA_LAH",
                String.valueOf(ar_no.get(i)) + "_"+
                String.valueOf(ar_nama.get(i))
            );
        }
        loadKeListView();
    }

    private void loadKeListView() {
        itemList.clear();
        for (int a=0; a<ar_no.size(); a++){
            DataItems dataItemsx = new DataItems();
            dataItemsx.setId(ar_no.get(a));
            dataItemsx.setNama(ar_nama.get(a));
            itemList.add(dataItemsx);
        }
        adapterItems.notifyDataSetChanged();
    }
}
