package com.nexsoft.firman.itemshop;

/**
 * Created by firmanmac on 3/13/17.
 */

public class DataItems {
    int Id;
    String nama;

    public DataItems() {
    }

    public DataItems(int id, String nama) {
        Id = id;
        this.nama = nama;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
