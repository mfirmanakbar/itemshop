package com.nexsoft.firman.itemshop;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

/**
 * Created by firmanmac on 3/13/17.
 */

public class AdapterItems extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List<DataItems> items;

    private Context context;

    private int angka_item = 0;

    private DataItems dataItemsx;
    TextView hasilnya;

    public AdapterItems(Activity activity, List<DataItems> items) {
        this.activity = activity;
        this.items = items;
        this.context = activity;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.items,null);
        }

        final TextView id = (TextView) convertView.findViewById(R.id.txtId);
        final TextView nama = (TextView) convertView.findViewById(R.id.txtNama);
        hasilnya = (TextView) convertView.findViewById(R.id.txtHasil);
        final Button btnKurang = (Button) convertView.findViewById(R.id.btnKurang);
        final Button btnTambah = (Button) convertView.findViewById(R.id.btnTambah);

        dataItemsx = items.get(position);
        id.setText(String.valueOf(dataItemsx.getId()));
        nama.setText(dataItemsx.getNama());
        hasilnya.setText("0");

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //hitung(true, position);
                hasilnya.setText("18");
            }
        });

        btnKurang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitung(false, position);
            }
        });

        return convertView;
    }

    private void hitung(boolean hit, int position) {
        //Toast.makeText(context,"Posisi: "+position+" & Operator:"+hit,Toast.LENGTH_SHORT).show();
//        if (hit==true && angka_item>=0){
//            angka_item+=angka_item+1;
//        }
//        if(hit==false && angka_item>0) {
//            angka_item+=angka_item-1;
//        }
//        hasilnya.setText(String.valueOf(angka_item));
    }
}
